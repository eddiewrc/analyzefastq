**USAGE INSTRUCTIONS**

This script will read a fastq file, computing statistics over the sgRNA guides provided in the guidesList.fasta file.

There are two ways to set the parameters. The recommended one is to use the command line, calling the python file as explained below.
Command line USAGE: 

`python analyzeResults.py guidesFastaFile fastqFile seqBeforeSgRNA seqAfterSsgRNA seqBeforeUMI seqAfterUMI minNumReadsForEachUmi umiLen guideLen`

Where:

* `guidesFastaFile ` path to the fasta file containing all the expected guides
* `fastqFile ` path to the fastq file containing all the reads
* `seqBeforeSgRNA ` short sequence of nucleotides that should precede the guide
* `seqAfterSsgRNA ` short sequence of nucleotides that should follow the guide
* `seqBeforeUMI ` short sequence of nucleotides that should precede the UMI
* `seqAfterUMI ` short sequence of nucleotides that should follow the UMI
* `minNumReadsForEachUmi ` minimum number of observed sgRNA+UMI counts to be considered (int)
* `umiLen` expected length of the UMI (int)
* `guideLen` expected length of the sgRNA (int)


Alternatively, you can set the parameters by modifying the variables at lines 26-33 and run the script with the following:

`python anayzeResults.py --noparams`


For example, the computation on the `testFolder` example files can be performed with the following command line:

`python analyzeResults.py testFiles/guidesList.fasta testFiles/readsFile.fastq GAAACACCG GTTTTAGA TTTTTTGC GCGAATTC 1 6 20`

In both approaches, the results (txt files in tab separated values format) will be reported in two files created in the same folder, called `./guideSummary1.txt`, `./guideSummary2.txt`.
The file `./guideSummaryOrdered.txt` presents the same data but in the same order in which the guides are provided in the `guidesFastaFile`.

