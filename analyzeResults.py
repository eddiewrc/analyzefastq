#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  analyzeResults.py
#  
#  Copyright 2021 eddiewrc <eddiewrc@alnilam>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
###########################################################
#GLOBAL VARIABLES: you can change these
guidesFile = "Kinase phosphatase activation library guides.fasta"
fastqFile = "UMI test file.fastq"
seqBeforeSg = "GAAACACCG"
seqAfterSg = "GTTTTAGA"
seqBeforeUmi = "TTTTTTGC"
seqAfterUmi = "GCGAATTC"
umiLen = 6
minNumUmiOcc = 2
guideLen = 20
#, but do not change the code BELOW THIS LINE!
############################################################

"""
These are the files that you asked. Just a short summary of what we need:

Each read in the .fastq file will contain a 20 nt sgRNA sequence and a 6 nt UMI sequence, flanked by the sequences indicated in the attachment with editable parameters.

We want to know for each sgRNA:

-          How many reads contain this sgRNA?
-          How many unique UMIs are found within the same read as this sgRNA?
-          How many reads contain each of the sgRNA-UMI combinations?

- sequence before sgRNA: GAAACACCG 
- sequence after sgRNA: GTTTTAGA 
(the sgRNA itself should be 20 nt long. If not, this sgRNA is not found in the list and is not counted. That is fine) 
- sequence before UMI: TTTTTTGC 
- sequence after UMI: GCGAATTC 
(the UMI should be 6 nt long. If not, something is wrong and the UMI should not be counted) 
 
- minimum number of reads for each UMI: 2 
(sgRNA-UMI combinations with <2 reads are ignored) 


"""

	
def main(args):
	print("Number of parameters: %d\nParameters: %s" %(len(args), str(args)))
	if len(args) == 1 or "-h" in args[1] or (len(args) != 10 and not "noparam" in args[1]) :
		print("\nCommand line USAGE: \n\npython analyzeResults.py guidesFastaFile fastqFile seqBeforeSgRNA seqAfterSsgRNA seqBeforeUMI seqAfterUMI minNumReadsForEachUmi umiLen guideLen\n")
		print("Alternatively, you can set the parameters by modifying the variables at lines 26-33 and run the script with the following:\n\npython anayzeResults.py --noparams\n")
		return 1

	global seqBeforeSg 
	global seqAfterSg 
	global seqBeforeUmi
	global seqAfterUmi 
	global umiLen  
	global guideLen	
	global minNumUmiOcc
	if not "noparam" in args[1]:
		guidesDB, lookup, order = readFASTA(args[1])
		fastqDB = readFastQ(args[2])
		seqBeforeSg = args[3]
		seqAfterSg = args[4]
		seqBeforeUmi = args[5]
		seqAfterUmi = args[6]
		minNumUmiOcc = int(args[7])
		umiLen = int(args[8])
		guideLen = int(args[9])
	else:
		guidesDB, lookup, order = readFASTA(guidesFile)
		fastqDB = readFastQ(fastqFile)

	#print order[:3]
	#print lookup.items()[:3]
	#print guidesDB.items()[:3]
	#raw
	resultsG = {}
	resultsGU = {}
	guideError = 0
	umiError = 0
	for s in fastqDB.items():
		print (s)
		#print(s[1].find(seqBeforeSg), s[1].find(seqAfterSg))
		guideStart = s[1].find(seqBeforeSg)+len(seqBeforeSg)
		#print(guideStart, guideStart+guideLen)
		#print(s[1])
		guide = s[1][guideStart:guideStart+guideLen]
		if len(guide) != guideLen:
			guideError += 1
			print("Guide %s is %d long instead of %d, skipping"% (guide, len(guide), guideLen))
		print(guide, len(guide))
		try:
			guideTag = lookup[guide]
		except:
			guideError += 1
			print("Guide %s is NOT PRESENT in the list of guides provided, skipping"% (guide))
			continue
		print(guideTag)
		umiStart = s[1].find(seqBeforeUmi)+len(seqBeforeUmi)
		umiEnd = s[1].find(seqAfterUmi)
		print(umiStart, umiEnd)
		if umiStart == -1 or umiEnd == -1:
			umiError += 1
			print ("Missing umi delimiters (%s, %s) in reads %s, skipping"%( seqBeforeUmi, seqAfterUmi, s[0]))
			continue
		umi = s[1][umiStart:umiEnd]
		if len(umi) != umiLen:
			umiError += 1
			print("Umi of read %s (%s) is %d long instead of %d, skipping."%(s[0], umi, len(umi), umiLen))
			continue
		print(umi)
		assert len(umi) == umiLen
		if not guideTag in resultsG:			
			resultsG[guideTag] = {"uniqueUmi":set(), "numReads":0, "sgUmi_combo":0}
		if not guideTag+":"+umi in resultsGU:
			resultsGU[guideTag+":"+umi] = 0
		resultsG[guideTag]["uniqueUmi"].add(umi)
		resultsG[guideTag]["numReads"] += 1
		resultsGU[guideTag+":"+umi] += 1 	
	print("Results printed in ./guideSummaryOrdered.txt , ./guideSummary1.txt and ./guideSummary2.txt")
	print("Number of reads skipped due to guides issues:%d, number of reads skipped due to UMI issues:%d" %(guideError, umiError))
	
	ofp = open("guideSummary2.txt","w")
	ofp.write("guideTag+UMI\toccurrences\n")
	filteredGU = {}
	for g in sorted(resultsGU.items(), key=lambda x :x[1], reverse=True):
		if g[1] >= minNumUmiOcc:
			if not g[0].split(":")[0] in filteredGU:
				filteredGU[g[0].split(":")[0]] = 0
			filteredGU[g[0].split(":")[0]] += 1
			ofp.write("%s\t%d\n"%(g[0], g[1]))
	ofp.close()
	
	ofp = open("guideSummaryOrdered.txt","w")
	ofp.write("guideTag\tguideSeq\tnumReads\tnumUnique_umi\tnumTimesAboveThreshold\n")
	for tag in order:
		if not tag in resultsG:
			ofp.write("%s\t%s\t%d\t%d\t%d\n"%(tag, guidesDB[tag], 0, 0, 0))
		else:
			f = 0
			if tag in filteredGU:
				f = filteredGU[tag]
			ofp.write("%s\t%s\t%d\t%d\t%d\n"%(tag, guidesDB[tag], resultsG[tag]["numReads"], len(resultsG[tag]["uniqueUmi"]), f))
	ofp.close()
	
	ofp = open("guideSummary1.txt","w")
	ofp.write("guideTag\tguideSeq\tnumReads\tnumUnique_umi\tnumTimesAboveThreshold\n")
	for g in resultsG.items():
		f = 0
		if g[0] in filteredGU:
			f = filteredGU[g[0]]
		ofp.write("%s\t%s\t%d\t%d\t%d\n"%(g[0], guidesDB[g[0]], g[1]["numReads"], len(g[1]["uniqueUmi"]), f))
	ofp.close()
	
	return 0

def readFASTA(seqFile, MIN_LENGTH = 4, MAX_LENGTH=999999999999):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	db = {}
	order = []
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line.replace(">","").strip(),""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:			
				order.append(tmp[0])
				sl[tmp[0]] = tmp[1]		
				db[tmp[1]] = tmp[0]
				if len(tmp[1]) != 20:
					print("ERROR: guide %s is %d nucleotides long!"% (tmp[0], len(tmp[1])))
				#db[tmp[1]] = tmp[0]	
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print ("Found %d GUIDES, added %d discarded %d" % (i, len(sl), discarded))
	return sl, db, order

def readFastQ(seqFile):
	db = {}
	ifp = open(seqFile)
	line = ifp.readline()
	while len(line) > 0:
		if line[0] == "@":
			name = line.strip()
			line = ifp.readline()
			seq = line.strip()
			db[name] = seq
		else:
			line = ifp.readline()
	print ("Found %d sequences in fast1 file" % len(db))
	return db

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
